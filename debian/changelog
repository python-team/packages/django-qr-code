django-qr-code (4.1.0-2) unstable; urgency=medium

  * added the file d/salsa-ci.yml
  * declared the dependendcy on python3-pytest in d/tests/control

 -- Georges Khaznadar <georgesk@debian.org>  Sat, 22 Jun 2024 15:34:09 +0200

django-qr-code (4.1.0-1) unstable; urgency=medium

  * New upstream version 4.1.0
  * refreshed debian patches
  * added a build-dependency: python3-pytest
  * bumped Standards-Version: 4.7.0

 -- Georges Khaznadar <georgesk@debian.org>  Tue, 04 Jun 2024 15:38:53 +0200

django-qr-code (3.1.1a-1) unstable; urgency=medium

  * replaced pytz by zoneinfo
  * adapted the debian patch: migrated from pytz to zoneinfo
  * marked 4 tests as skipped
  * upload to unstable

 -- Georges Khaznadar <georgesk@debian.org>  Mon, 16 Oct 2023 09:20:54 +0200

django-qr-code (3.1.1-2) experimental; urgency=medium

  * added a build dependency on python3-segno

 -- Georges Khaznadar <georgesk@debian.org>  Sun, 11 Jun 2023 11:20:36 +0200

django-qr-code (3.1.1-1) experimental; urgency=medium

  * New upstream version 3.1.1
  * adopted the package, Closes: #973705
  * added a new build-dependency on python3-pydantic
  * relaxed the version dependency for python3-django, created a versionned
    dependency on python3-segno
  * made a short patch for the file qr_code/qrcode/utils.py to disable
    the decorator @pydantic_dataclass once, as it makes the automated
    tests fail. This is a quick and dirty fix, it should be improved!
  * modified d/rules so django_qr_code.egg-info/ is cleaned

 -- Georges Khaznadar <georgesk@debian.org>  Fri, 21 Apr 2023 14:23:47 +0200

django-qr-code (2.2.0-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Contact.
  * Update standards version to 4.6.1, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 02 Nov 2022 21:01:25 +0000

django-qr-code (2.2.0-1) unstable; urgency=medium

  * New upstream version 2.2.0.

 -- Mattia Rizzolo <mattia@debian.org>  Sun, 18 Jul 2021 11:45:54 +0200

django-qr-code (2.1.0-1) unstable; urgency=medium

  [ Mattia Rizzolo ]
  * New upstream version 2.1.0.

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit,
    Repository, Repository-Browse.

 -- Mattia Rizzolo <mattia@debian.org>  Wed, 27 Jan 2021 19:34:54 +0100

django-qr-code (2.0.1-1) unstable; urgency=medium

  * New upstream version 2.0.1.
  * Remove patch applied upstream.
  * d/control:
    + Bump Standards-Version to 4.5.1, no changes needed.
    + Require django >= 2.2.

 -- Mattia Rizzolo <mattia@debian.org>  Tue, 24 Nov 2020 10:39:14 +0100

django-qr-code (2.0.0-1) unstable; urgency=medium

  * New upstream version 2.0.0.
  * Switch Build-Depends from python3-qrcode to python3-segno.
  * Add patch to fix setup.py's install_requires after a dependency change.

 -- Mattia Rizzolo <mattia@debian.org>  Sun, 22 Nov 2020 20:53:48 +0100

django-qr-code (1.3.1-2) unstable; urgency=medium

  * Drop unused Build-Depends.
  * Add an autopkgtest.

 -- Mattia Rizzolo <mattia@debian.org>  Tue, 03 Nov 2020 17:27:55 +0100

django-qr-code (1.3.1-1) unstable; urgency=medium

  * Initial release.

 -- Mattia Rizzolo <mattia@debian.org>  Tue, 13 Oct 2020 11:17:49 +0200
